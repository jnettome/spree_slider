class Spree::Slide < ActiveRecord::Base
  has_attached_file :image,
    :storage => :s3,
    :s3_credentials => "#{Rails.root}/config/s3.yml",
    :bucket => 'sexyone-products',
    :url => ":s3_domain_url",
    :path => "/:class/sliders/:id_:basename.:style.:extension"
  
  scope :published, where(:published => true)
  attr_accessible :name, :body, :link_url, :published, :image, :position
end